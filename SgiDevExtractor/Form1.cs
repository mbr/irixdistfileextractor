﻿using System;
using System.IO.Compression;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib;

namespace SgiDevExtractor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.DoWork += Worker;
            backgroundWorker1.RunWorkerCompleted += (s2, e2) =>
            {
                button2.Text = "Perform";
                button2.Enabled = true;

                if( e2.Error != null )
                {
                    MessageBox.Show(
                        e2.Error.Message,
                        "Error while extracting"
                    );
                }
            };
            backgroundWorker1.ProgressChanged += (s2, e2) =>
            {
                progressBar1.Value = e2.ProgressPercentage;
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var fc = new OpenFileDialog();
            fc.ShowDialog();

            textBox2.Text = fc.FileName;
        }


        public void Worker(object sender, DoWorkEventArgs e)
        {
            var index = System.IO.File.ReadAllLines(textBox2.Text + ".idb");
            var data = System.IO.File.ReadAllBytes(textBox2.Text + ".dev");
            int dataLoc = 0, curLine = 0;

            foreach (var line in index)
            {
                // Cancel?
                if (backgroundWorker1.CancellationPending)
                    break;

                var split = line.Split(' ');
                var props = new Dictionary<string, Int64>();

                if (split[0] != "f")
                    continue;

                for (int i = 6; i < split.Length; i++)
                {
                    var m = System.Text.RegularExpressions.Regex.Match(split[i], @"([a-z]+)\(([0-9]+)\)");

                    if (m.Groups.Count == 3)
                    {
                        props.Add(m.Groups[1].Value, Int64.Parse(m.Groups[2].Value));
                    }
                }


                var name = split[4];

                while (Encoding.ASCII.GetString(data, dataLoc, name.Length) != name)
                    dataLoc++;

                dataLoc += name.Length;

                var newDir = textBox3.Text + @"\" + name.Replace("/", @"\");
                var newFolder = System.IO.Path.GetDirectoryName(newDir);
                var newName = System.IO.Path.GetFileName(newDir);

                System.IO.Directory.CreateDirectory(newFolder);

                if (props["cmpsize"] > 0)
                {
                    var fileBytes = new byte[(int)props["cmpsize"]];
                    Array.Copy(data, dataLoc, fileBytes, 0, (int)props["cmpsize"]);

                    System.IO.File.WriteAllBytes(newFolder + @"\" + newName + ".Z", fileBytes);
                }
                else
                {
                    var fileBytes = new byte[(int)props["size"]];
                    Array.Copy(data, dataLoc, fileBytes, 0, (int)props["size"]);

                    System.IO.File.WriteAllBytes(newFolder + @"\" + newName, fileBytes);
                }

                textBox1.Invoke(
                    new MethodInvoker(delegate()
                    {
                        textBox1.AppendText(name + Environment.NewLine);
                    })
                );
                //textBox1.AppendText(String.Format("0x{0:X16}", props["f"]));
                

                curLine++;

                // Report progress
                backgroundWorker1.ReportProgress((int)((double)curLine / (double)index.Length * 100.0));
            }

            backgroundWorker1.ReportProgress(100);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                (sender as Button).Enabled = false;
            }
            else
            {
                textBox1.Clear();
                (sender as Button).Text = "Stop";
                backgroundWorker1.RunWorkerAsync();
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var fc = new FolderBrowserDialog();
            fc.ShowDialog();
            textBox3.Text = fc.SelectedPath;
        }
    }
}
